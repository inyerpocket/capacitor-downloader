export enum StatusCode {
  PENDING = 'pending',
  PAUSED = 'paused',
  DOWNLOADING = 'downloading',
  COMPLETED = 'completed',
  ERROR = 'error'
}

export type CallbackID = string;
export type DownloadID = string;

export interface RequestOptions {
  id: DownloadID;
}

export interface TimeoutOptions {
  timeout: number;
}

export interface CreateOptions {
  url: string;
  query?: Object | string;
  headers?: Object;
  path?: string;
  fileName?: string;
}

export interface Progress {
  status?: StatusCode;
  progress?: number;
  currentSize?: number;
  totalSize?: number;
  speed?: number;
  path?: string;
}

export type ProgressCallback = (
  progress: Progress | null,
  err?: any,
) => void;

export interface DownloaderPluginCapacitor {
  initialize(): Promise<void>;
  setTimeout(options: TimeoutOptions): Promise<void>;
  create(options: CreateOptions): Promise<object>;
  start(options: RequestOptions, callback?: ProgressCallback): Promise<CallbackID>;
  pause(options: RequestOptions): Promise<void>;
  resume(options: RequestOptions): Promise<void>;
  cancel(options:RequestOptions): Promise<void>;
  getPath(options: RequestOptions): Promise<object>;
  getStatus(options: RequestOptions): Promise<object>;

}

export interface DownloaderPluginWrapper {
  initialize(): Promise<void>;
  setTimeout(options: TimeoutOptions): Promise<void>;
  create(options: CreateOptions): Promise<object>;
  start(options: RequestOptions, progress?: ProgressCallback): Promise<Progress>;
  pause(options: RequestOptions): Promise<void>;
  resume(options: RequestOptions): Promise<void>;
  cancel(options: RequestOptions): Promise<void>;
  getPath(options: RequestOptions): Promise<object>;
  getStatus(options: RequestOptions): Promise<object>;
}
