import { registerPlugin } from '@capacitor/core';

import type { CreateOptions, DownloaderPluginCapacitor, DownloaderPluginWrapper, Progress, ProgressCallback, RequestOptions, TimeoutOptions } from './definitions';

const Downloader = registerPlugin<DownloaderPluginCapacitor>('Downloader', {
  web: () => import('./web').then(m => new m.DownloaderWeb()),
  electron: () => (window as any).CapacitorCustomPlatform.plugins.Downloader,
});

export * from './definitions';
export { Downloader };

class DownloaderWrapper implements DownloaderPluginWrapper {
  public async initialize(): Promise<void> {
    return Downloader.initialize();
  }

  public async setTimeout(options: TimeoutOptions): Promise<void> {
    return Downloader.setTimeout(options);
  }
  
  public async create(options: CreateOptions): Promise<object> {
    return Downloader.create(options);
  }

  public start(options: RequestOptions, progress?: ProgressCallback): Promise<Progress> {
    return new Promise((resolve, reject) => {
      // @ts-ignore
      if(window.CapacitorCustomPlatform) {
        Downloader.start(options).then((callbackId) => {
          let eventId: any;
          const callback = (data: any, error: any) => {
            if (!error) {
              if (data && data.status != null) {
                resolve(data);
                // @ts-ignore
                window.CapacitorCustomPlatform.plugins.Downloader.removeListener(eventId);
              } else if (data && progress) {
                progress(data);
              }
            } else {
              reject(error);
              // @ts-ignore
              window.CapacitorCustomPlatform.plugins.Downloader.removeListener(eventId);
            }
          };

          // @ts-ignore
          if(window.CapacitorCustomPlatform.plugins.Downloader) {
            // @ts-ignore
            eventId = window.CapacitorCustomPlatform.plugins.Downloader.addListener(callbackId, callback);
          }
        });
      } else {
        Downloader.start(options, (data, error) => {
          if (!error) {
            if (data && data.status != null) {
              resolve(data);
            } else if (data && progress) {
              progress(data);
            }
          } else {
            reject(error);
          }
        });
      }
    });
  }

  public async pause(options: RequestOptions): Promise<void> {
    return Downloader.pause(options);
  }

  public async resume(options: RequestOptions): Promise<void> {
    return Downloader.resume(options);
  }

  public async cancel(options: RequestOptions): Promise<void> {
    return Downloader.cancel(options);
  }

  public async getPath(options: RequestOptions): Promise<object> {
    return Downloader.getPath(options);
  }

  public async getStatus(options: RequestOptions): Promise<object> {
    return Downloader.getStatus(options);
  }
}

const DownloaderPlugin = new DownloaderWrapper();

export { DownloaderPlugin };