import { WebPlugin } from '@capacitor/core';

import type { CallbackID, CreateOptions, DownloaderPluginCapacitor, RequestOptions, TimeoutOptions } from './definitions';

export class DownloaderWeb extends WebPlugin implements DownloaderPluginCapacitor {
  async initialize(): Promise<void> {
    throw "not implemented in web";
  }
  
  async setTimeout(options: TimeoutOptions): Promise<void> {
    console.error("not implemented in web", options);
    throw "not implemented in web";
  }

  async create(options: CreateOptions): Promise<object> {
    console.error("not implemented in web", options);
    throw "not implemented in web";
  }

  async start(options: RequestOptions): Promise<CallbackID> {
    console.error("not implemented in web", options);
    throw "not implemented in web";
  }

  async pause(options: RequestOptions): Promise<void> {
    console.error("not implemented in web", options);
    throw "not implemented in web";
  }

  async resume(options: RequestOptions): Promise<void> {
    console.error("not implemented in web", options);
    throw "not implemented in web";
  }

  async cancel(options: RequestOptions): Promise<void> {
    console.error("not implemented in web", options);
    throw "not implemented in web";
  }

  async getPath(options: RequestOptions): Promise<object> {
    console.error("not implemented in web", options);
    throw "not implemented in web";
  }

  async getStatus(options: RequestOptions): Promise<object> {
    console.error("not implemented in web", options);
    throw "not implemented in web";
  }
}
