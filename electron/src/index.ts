/* eslint-disable @typescript-eslint/no-unused-vars */
const NodeRequest = require('request');
const NodeFs = require("fs");
import { EventEmitter } from 'events';

import { CallbackID, CreateOptions, DownloaderPluginCapacitor, RequestOptions, StatusCode, TimeoutOptions } from '../../src/definitions';

export class Downloader extends EventEmitter implements DownloaderPluginCapacitor {
  downloads: any = {};
  timeout: number = 60;

  async initialize(): Promise<void> {
    return;
  }

  async setTimeout(options: TimeoutOptions): Promise<void> {
    this.timeout = options.timeout;
    return;
  }
  
  async create(options: CreateOptions): Promise<object> {
    let dlId = `i-${(new Date()).getTime()}`;

    this.downloads[dlId] = {status: StatusCode.PENDING, path: options.path, fileName: options.fileName, url: options.url};

    return {id: dlId};
  }

  start(options: RequestOptions): Promise<CallbackID> {
    return new Promise((resolve) => {
      const callbackId = `dlc-${(new Date()).getTime()}`;
      resolve(callbackId);

      setTimeout(() => {
        if(!options || !options.id) {
          this.emit(callbackId, null, "must set options");
          return;
        }

        const downloadData = this.downloads[options.id];

        if(!downloadData) {
          this.emit(callbackId, null, "no valid download found");
          return;
        }

        let fileReceivedBytes = 0;
        let fileTotalBytes = 0;
        const filePath = `${downloadData.path}/${downloadData.fileName}`;
        const file = NodeFs.createWriteStream(filePath);

        NodeRequest.get(downloadData.url)
        .on('error', (err: any) => {
          NodeFs.unlink(filePath, () => {
            this.emit(callbackId, null, err.message);
          });
        })
        .on('response', (data: any) => {
          fileTotalBytes = parseInt(data.headers['content-length']);
        })
        .on('data', (chunk: any) => {
          fileReceivedBytes += chunk.length;
          this.emit(callbackId, {
            progress: (fileReceivedBytes * 100 / fileTotalBytes),
            currentSize: fileReceivedBytes,
            totalSize: fileTotalBytes,
            speed: 0
          });
        })
        .pipe(file)
        .on('finish', () => {
            this.emit(callbackId, {status: StatusCode.COMPLETED, path: downloadData.path});
        })
        .on('error', (err: any) => {
          NodeFs.unlink(filePath, () => {
            this.emit(callbackId, null, err.message);
          });
        });

        file.on('error', (err: any) => { // Handle errors
          NodeFs.unlink(filePath, () => {
            this.emit(callbackId, null, err.message);
          });
        });
      }, 100);
    });
  }

  async pause(options: RequestOptions): Promise<void> {
    console.error("not implemented in electron", options);
    throw "not implemented in electron";
  }

  async resume(options: RequestOptions): Promise<void> {
    console.error("not implemented in electron", options);
    throw "not implemented in electron";
  }

  async cancel(options: RequestOptions): Promise<void> {
    console.error("not implemented in electron", options);
    throw "not implemented in electron";
  }

  async getPath(options: RequestOptions): Promise<object> {
    if(!this.downloads[options.id]) {
      throw "no valid download found";
    }

    return {path: this.downloads[options.id].path};
  }

  async getStatus(options: RequestOptions): Promise<object> {
    if(!this.downloads[options.id]) {
      throw "no valid download found";
    }

    return {status: this.downloads[options.id].status};
  }
}
