import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import json from "@rollup/plugin-json";

export default {
  input: 'electron/build/electron/src/index.js',
  output: {
    file: 'electron/dist/plugin.js',
    format: 'cjs',
    sourcemap: true
  },
  external: ['@capacitor/core', 'path', 'fs', 'os'],
  plugins: [json(), resolve(), commonjs()]
};
