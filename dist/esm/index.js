import { registerPlugin } from '@capacitor/core';
const Downloader = registerPlugin('Downloader', {
    web: () => import('./web').then(m => new m.DownloaderWeb()),
    electron: () => window.CapacitorCustomPlatform.plugins.Downloader,
});
export * from './definitions';
export { Downloader };
class DownloaderWrapper {
    async initialize() {
        return Downloader.initialize();
    }
    async setTimeout(options) {
        return Downloader.setTimeout(options);
    }
    async create(options) {
        return Downloader.create(options);
    }
    start(options, progress) {
        return new Promise((resolve, reject) => {
            // @ts-ignore
            if (window.CapacitorCustomPlatform) {
                Downloader.start(options).then((callbackId) => {
                    let eventId;
                    const callback = (data, error) => {
                        if (!error) {
                            if (data && data.status != null) {
                                resolve(data);
                                // @ts-ignore
                                window.CapacitorCustomPlatform.plugins.Downloader.removeListener(eventId);
                            }
                            else if (data && progress) {
                                progress(data);
                            }
                        }
                        else {
                            reject(error);
                            // @ts-ignore
                            window.CapacitorCustomPlatform.plugins.Downloader.removeListener(eventId);
                        }
                    };
                    // @ts-ignore
                    if (window.CapacitorCustomPlatform.plugins.Downloader) {
                        // @ts-ignore
                        eventId = window.CapacitorCustomPlatform.plugins.Downloader.addListener(callbackId, callback);
                    }
                });
            }
            else {
                Downloader.start(options, (data, error) => {
                    if (!error) {
                        if (data && data.status != null) {
                            resolve(data);
                        }
                        else if (data && progress) {
                            progress(data);
                        }
                    }
                    else {
                        reject(error);
                    }
                });
            }
        });
    }
    async pause(options) {
        return Downloader.pause(options);
    }
    async resume(options) {
        return Downloader.resume(options);
    }
    async cancel(options) {
        return Downloader.cancel(options);
    }
    async getPath(options) {
        return Downloader.getPath(options);
    }
    async getStatus(options) {
        return Downloader.getStatus(options);
    }
}
const DownloaderPlugin = new DownloaderWrapper();
export { DownloaderPlugin };
//# sourceMappingURL=index.js.map