import { WebPlugin } from '@capacitor/core';
import type { CallbackID, CreateOptions, DownloaderPluginCapacitor, RequestOptions, TimeoutOptions } from './definitions';
export declare class DownloaderWeb extends WebPlugin implements DownloaderPluginCapacitor {
    initialize(): Promise<void>;
    setTimeout(options: TimeoutOptions): Promise<void>;
    create(options: CreateOptions): Promise<object>;
    start(options: RequestOptions): Promise<CallbackID>;
    pause(options: RequestOptions): Promise<void>;
    resume(options: RequestOptions): Promise<void>;
    cancel(options: RequestOptions): Promise<void>;
    getPath(options: RequestOptions): Promise<object>;
    getStatus(options: RequestOptions): Promise<object>;
}
