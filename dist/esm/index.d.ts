import type { CreateOptions, DownloaderPluginCapacitor, DownloaderPluginWrapper, Progress, ProgressCallback, RequestOptions, TimeoutOptions } from './definitions';
declare const Downloader: DownloaderPluginCapacitor;
export * from './definitions';
export { Downloader };
declare class DownloaderWrapper implements DownloaderPluginWrapper {
    initialize(): Promise<void>;
    setTimeout(options: TimeoutOptions): Promise<void>;
    create(options: CreateOptions): Promise<object>;
    start(options: RequestOptions, progress?: ProgressCallback): Promise<Progress>;
    pause(options: RequestOptions): Promise<void>;
    resume(options: RequestOptions): Promise<void>;
    cancel(options: RequestOptions): Promise<void>;
    getPath(options: RequestOptions): Promise<object>;
    getStatus(options: RequestOptions): Promise<object>;
}
declare const DownloaderPlugin: DownloaderWrapper;
export { DownloaderPlugin };
