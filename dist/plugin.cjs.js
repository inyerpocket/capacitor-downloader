'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var core = require('@capacitor/core');

exports.StatusCode = void 0;
(function (StatusCode) {
    StatusCode["PENDING"] = "pending";
    StatusCode["PAUSED"] = "paused";
    StatusCode["DOWNLOADING"] = "downloading";
    StatusCode["COMPLETED"] = "completed";
    StatusCode["ERROR"] = "error";
})(exports.StatusCode || (exports.StatusCode = {}));

const Downloader = core.registerPlugin('Downloader', {
    web: () => Promise.resolve().then(function () { return web; }).then(m => new m.DownloaderWeb()),
    electron: () => window.CapacitorCustomPlatform.plugins.Downloader,
});
class DownloaderWrapper {
    async initialize() {
        return Downloader.initialize();
    }
    async setTimeout(options) {
        return Downloader.setTimeout(options);
    }
    async create(options) {
        return Downloader.create(options);
    }
    start(options, progress) {
        return new Promise((resolve, reject) => {
            // @ts-ignore
            if (window.CapacitorCustomPlatform) {
                Downloader.start(options).then((callbackId) => {
                    let eventId;
                    const callback = (data, error) => {
                        if (!error) {
                            if (data && data.status != null) {
                                resolve(data);
                                // @ts-ignore
                                window.CapacitorCustomPlatform.plugins.Downloader.removeListener(eventId);
                            }
                            else if (data && progress) {
                                progress(data);
                            }
                        }
                        else {
                            reject(error);
                            // @ts-ignore
                            window.CapacitorCustomPlatform.plugins.Downloader.removeListener(eventId);
                        }
                    };
                    // @ts-ignore
                    if (window.CapacitorCustomPlatform.plugins.Downloader) {
                        // @ts-ignore
                        eventId = window.CapacitorCustomPlatform.plugins.Downloader.addListener(callbackId, callback);
                    }
                });
            }
            else {
                Downloader.start(options, (data, error) => {
                    if (!error) {
                        if (data && data.status != null) {
                            resolve(data);
                        }
                        else if (data && progress) {
                            progress(data);
                        }
                    }
                    else {
                        reject(error);
                    }
                });
            }
        });
    }
    async pause(options) {
        return Downloader.pause(options);
    }
    async resume(options) {
        return Downloader.resume(options);
    }
    async cancel(options) {
        return Downloader.cancel(options);
    }
    async getPath(options) {
        return Downloader.getPath(options);
    }
    async getStatus(options) {
        return Downloader.getStatus(options);
    }
}
const DownloaderPlugin = new DownloaderWrapper();

class DownloaderWeb extends core.WebPlugin {
    async initialize() {
        throw "not implemented in web";
    }
    async setTimeout(options) {
        console.error("not implemented in web", options);
        throw "not implemented in web";
    }
    async create(options) {
        console.error("not implemented in web", options);
        throw "not implemented in web";
    }
    async start(options) {
        console.error("not implemented in web", options);
        throw "not implemented in web";
    }
    async pause(options) {
        console.error("not implemented in web", options);
        throw "not implemented in web";
    }
    async resume(options) {
        console.error("not implemented in web", options);
        throw "not implemented in web";
    }
    async cancel(options) {
        console.error("not implemented in web", options);
        throw "not implemented in web";
    }
    async getPath(options) {
        console.error("not implemented in web", options);
        throw "not implemented in web";
    }
    async getStatus(options) {
        console.error("not implemented in web", options);
        throw "not implemented in web";
    }
}

var web = /*#__PURE__*/Object.freeze({
    __proto__: null,
    DownloaderWeb: DownloaderWeb
});

exports.Downloader = Downloader;
exports.DownloaderPlugin = DownloaderPlugin;
//# sourceMappingURL=plugin.cjs.js.map
